
public class TheaterManagement {
	private Seat seat;
	private TheaterSystemGUI gui;

	
	public TheaterManagement(Seat seat,TheaterSystemGUI gui){
		this.seat = seat;
		this.gui=gui;
		gui.setController(this);
	}
	
	
	public double[][] getSeatPrice(){
		return seat.getDataSeatPrice();
	}
	
	

}
