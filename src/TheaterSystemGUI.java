import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;





public class TheaterSystemGUI {
	private TheaterManagement controller;
	private String str="ABCDEFGHILMNOPQ";
	private double totalPrice;
	private boolean isFirstClick = true;
	private int i;
	private int j;

	
	public void setController(TheaterManagement controller){
		this.controller = controller;

	}
	
	public TheaterSystemGUI(){
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setSize(520,200);
		frame.setTitle("THEATER MANAGEMENT");
		frame.setLayout(null);

		JTextField welcome = new JTextField("WELCOME");
		welcome.setForeground (new Color(0,0,0));
		welcome.setBackground (new Color(252, 250, 139));
		welcome.setBounds(10, 10, 470, 45);
		welcome.setHorizontalAlignment(JTextField.CENTER);
		frame.add(welcome);
		
		JLabel timeLabel = new JLabel("Select Time");
		timeLabel.setBounds(20, 80, 80, 30);
		timeLabel.setHorizontalAlignment(JTextField.CENTER);
		frame.add(timeLabel);
		
		JComboBox timeList = new JComboBox();
		timeList.setBounds(100, 80, 200, 30);
		timeList.addItem("Monday time 14.00-15.00");
		timeList.addItem("Monday time 16.00-18.00");
		
		timeList.addItem("Tuesday time 14.00-15.00");
		timeList.addItem("Tuesday time 16.00-18.00");
		
		timeList.addItem("Wednesday time 14.00-15.00");
		timeList.addItem("Wednesday time 16.00-18.00");
		
		timeList.addItem("Thursday time 14.00-15.00");
		timeList.addItem("Thursday time 16.00-18.00");
		
		timeList.addItem("Friday time 14.00-15.00");
		timeList.addItem("Friday time 16.00-18.00");
		
		timeList.addItem("Saterday time 14.00-15.00");
		timeList.addItem("Saterday time 16.00-18.00");
		timeList.addItem("Saterday time 19.00-20.00");
		
		timeList.addItem("Sunday time 14.00-15.00");
		timeList.addItem("Sunday time 16.00-18.00");
		timeList.addItem("Sunday time 19.00-20.00");
		timeList.setSelectedIndex(0);
		frame.add(timeList);

		JButton selectTime = new JButton("Select");
		selectTime.setBounds(420, 80, 80, 30);
		selectTime.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				
				JFrame frame2 = new JFrame();
				frame2.setResizable(false);
				frame2.setSize(1280, 600);
				frame2.setTitle("THEATER MANAGEMENT");
				frame2.setLayout(null);
				
				JLabel[] columnLabel = new JLabel[20];
				for(int i = 0; i < 20; i++)
				{
					columnLabel[i] = new JLabel("" + (i+1));
					columnLabel[i].setBounds(80 + (i * 56), 70, 40, 24);
					columnLabel[i].setForeground(new Color(221, 160, 221));
					columnLabel[i].setHorizontalAlignment(JTextField.CENTER);
					frame2.add(columnLabel[i]);
				}
				
				
				JLabel[] rowLabel = new JLabel[15];
				for(int i = 0; i < 15; i++) {
					rowLabel[i] = new JLabel("" + String.valueOf(str.charAt(i)));
					rowLabel[i].setBounds(30, 110 + (i * 35), 40, 40);
					rowLabel[i].setForeground(new Color(221, 160, 221));
					rowLabel[i].setHorizontalAlignment(JTextField.CENTER);
					frame2.add(rowLabel[i]);
				}

				JButton[][] seatBtn = new JButton[15][20];
				for(i=0; i < 13; i++) {
					for(j=0; j < 20; j++) {
						seatBtn[i][j] = new JButton("" + (int)controller.getSeatPrice()[i][j]);
						seatBtn[i][j].setBounds(80 + (j * 56), 110 + (i * 35), 50, 33);
						seatBtn[i][j].setBackground(Color.white);
						seatBtn[i][j].setHorizontalAlignment(JTextField.CENTER);
						seatBtn[i][j].putClientProperty("column", i);
						seatBtn[i][j].putClientProperty("row", j);
						seatBtn[i][j].addActionListener(new ActionListener() {
							
							@Override
							public void actionPerformed(ActionEvent e) {
								// TODO Auto-generated method stub
							    JButton btn = (JButton) e.getSource();
								if (seatBtn[(int) btn.getClientProperty("column")][ (int) btn.getClientProperty("row")].getBackground()!=Color.red){
									  
							        if (seatBtn[(int) btn.getClientProperty("column")][ (int) btn.getClientProperty("row")].isEnabled()) {
							        	seatBtn[(int) btn.getClientProperty("column")][ (int) btn.getClientProperty("row")].setBackground(Color.red);
							        	totalPrice += controller.getSeatPrice()[ (int) btn.getClientProperty("column")][ (int) btn.getClientProperty("row")];
							        }
								}


							}
						});
						
		
						if(controller.getSeatPrice()[i][j] != -1.0) {
							frame2.add(seatBtn[i][j]);
						}
					}
				}
				
				JLabel priceLabel = new JLabel("Total Price :");
				priceLabel.setBounds(300, 10, 80, 30);
				priceLabel.setHorizontalAlignment(JTextField.CENTER);
				frame2.add(priceLabel);

				JTextField priceField = new JTextField();
				priceField.setBounds(400, 10, 75, 30);
				priceField.setHorizontalAlignment(JTextField.CENTER);
				priceField.setForeground(Color.black);
				priceField.setText(""+totalPrice);
				frame2.add(priceField);
				
				JButton submit = new JButton("Submit");
				submit.setBounds(200, 10, 75, 30);
				submit.setHorizontalAlignment(JTextField.CENTER);
				frame2.add(submit);
				submit.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						// TODO Auto-generated method stub
						priceField.setText(""+totalPrice);
					}
				});
				
				JLabel price = new JLabel("Price :");
				price.setBounds(530,10,100,30);
		//		price.setForeground(Color.black);
				frame2.add(price);
				
				
				
				JComboBox priceList = new JComboBox();
				priceList.setBounds(570, 10, 50, 30);
				priceList.setForeground(Color.black);
				priceList.addItem("10");
				priceList.addItem("20");
				priceList.addItem("30");
				priceList.addItem("40");
				priceList.addItem("50");
				frame2.add(priceList);
				
	
				
				JButton searchPrice = new JButton("Buy");
				searchPrice.setBounds(640, 10, 75, 30);
				searchPrice.setHorizontalAlignment(JTextField.CENTER);
				frame2.add(searchPrice);
				searchPrice.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						
						System.out.println(""+i);
						System.out.println(""+j);
						if (seatBtn[i][j].getBackground() == Color.red) {
							JFrame frame3 = new JFrame();
							frame3.setResizable(false);
							frame3.setBounds(450, 250, 400, 200);
							frame3.setTitle("Warning");
							frame3.setLayout(null);
							JLabel caution = new JLabel("This seat is already taken");
							caution.setBounds(120,60,300,30);
							frame3.add(caution);
							JLabel caution2 = new JLabel("Please select other seat");
							caution2.setBounds(125,80,300,30);
							frame3.add(caution2);
							
							frame3.setVisible(true);
						}
						else{
							seatBtn[i][j].setBackground(Color.red);
							totalPrice += controller.getSeatPrice()[i][j];
							priceField.setText(""+totalPrice);
							}
					}
				});
				
				
				
				frame2.setVisible(true);

			}
			
		});
		
		frame.add(selectTime);
		
		frame.setVisible(true);
	}
}
